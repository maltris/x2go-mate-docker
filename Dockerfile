FROM	ubuntu:zesty

MAINTAINER morph027

ENV	DEBIAN_FRONTEND noninteractive

## disable ipv6

RUN	echo 'net.ipv6.conf.default.disable_ipv6 = 1' > /etc/sysctl.d/20-ipv6-disable.conf; \
	echo 'net.ipv6.conf.all.disable_ipv6 = 1' >> /etc/sysctl.d/20-ipv6-disable.conf; \
	echo 'net.ipv6.conf.lo.disable_ipv6 = 1' >> /etc/sysctl.d/20-ipv6-disable.conf; \
	cat /etc/sysctl.d/20-ipv6-disable.conf; sysctl -p

## package installation

RUN	apt-get update && \
	apt-get install -y software-properties-common && \
	add-apt-repository -y ppa:x2go/stable && \
	add-apt-repository -y universe && \
	add-apt-repository -y multiverse && \
	apt-get update && \
	apt-get install -y \
		consolekit \
		openssh-server \
		x2goserver \
		x2goserver-xsession \
		x2gomatebindings \
		dconf-cli \
		mate-desktop \
		mate-backgrounds \
		mate-applets \
		mate-applet-brisk-menu \
		mate-control-center \
		mate-tweak \
		mate-themes \
		mate-terminal \
		mate-settings-daemon \
		mate-screensaver \
		mate-polkit \
		mate-panel \
		mate-menu \
		mate-indicator-applet \
		moka-icon-theme \
		faba-icon-theme \
		arc-theme \
		firefox \
		bash-completion \
		sudo \
		inetutils-syslogd \
		vim \
		curl \
	&& apt-get clean && rm -rf /var/lib/apt/lists/* /var/cache/apt/*

## bashrc color tweaks

RUN	sed -i 's,^#force_color_prompt,force_color_prompt,' /etc/skel/.bashrc && \
	sed 's,01;32m,01;31m,' /etc/skel/.bashrc > /root/.bashrc

## add a user

RUN	useradd -s /bin/bash -G sudo -m x2go && \
	echo 'x2go:x2go' | chpasswd

## add tini to cleanly handle sub-processes

ENV	TINI_VERSION v0.16.1
ADD	https://github.com/krallin/tini/releases/download/${TINI_VERSION}/tini /tini
RUN	chmod +x /tini
ENTRYPOINT	["/tini", "--"]

COPY	entrypoint.sh /entrypoint.sh
RUN	chmod +x /entrypoint.sh
CMD	["/entrypoint.sh"]

## fix gnome keyring startup

RUN	sed -i 's,/usr/bin/gnome-keyring-daemon --start --components=secrets,/bin/sleep 5 && /usr/bin/gnome-keyring-daemon --start,' /etc/xdg/autostart/gnome-keyring-secrets.desktop

## fix problems with x2go extensions

RUN	sed -i 's,X2GO_NXAGENT_DEFAULT_OPTIONS+=" -extension XFIXES",#X2GO_NXAGENT_DEFAULT_OPTIONS+=" -extension XFIXES",' /etc/x2go/x2goagent.options

RUN	sed -i 's,#X2GO_NXAGENT_DEFAULT_OPTIONS+=" -extension GLX",X2GO_NXAGENT_DEFAULT_OPTIONS+=" -extension GLX",' /etc/x2go/x2goagent.options

## Chrome

RUN	curl -sL https://dl-ssl.google.com/linux/linux_signing_key.pub | apt-key add - \
	&& echo "deb http://dl.google.com/linux/chrome/deb/ stable main" >> /etc/apt/sources.list.d/google-chrome.list \
	&& apt-get update -y \
	&& apt-get -y install google-chrome-stable \
	&& rm /etc/apt/sources.list.d/google-chrome.list \
	&& rm -rf /var/lib/apt/lists/* /var/cache/apt/*

## fix chrome stuff (see https://github.com/SeleniumHQ/docker-selenium/blob/master/NodeChrome/Dockerfile.txt)

COPY	chrome_launcher.sh /opt/google/chrome/google-chrome
RUN	chmod +x /opt/google/chrome/google-chrome

RUN	sed -i 's,/usr/bin/google-chrome-stable,/opt/google/chrome/google-chrome,g' /usr/share/applications/google-chrome.desktop


# Following line fixes
# https://github.com/SeleniumHQ/docker-selenium/issues/87
ENV	DBUS_SESSION_BUS_ADDRESS=/dev/null

## add theming and misc. desktop dconf settings

ADD	dconf /etc/dconf

RUN	dconf update

EXPOSE	22
