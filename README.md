# X2Go Mate Desktop inside Docker

Just run a [X2Go](http://wiki.x2go.org/doku.php) server with [Mate Desktop](http://wiki.mate-desktop.org/) preinstalled inside Docker. You can login with user/password ```x2go```. Feel free to fork and modify settings (e.g. login, packages, ...).

This image is being shipped with the beautiful [Arc GTK Theme](https://github.com/horst3180/Arc-theme) and [Moka Icon Theme](https://snwh.org/moka).

![](https://gitlab.com/morph027/x2go-mate-docker/raw/master/x2go-mate-docker.png)

## Usage

 :exclamation: To have dbus and stuff running, we need to run the container in ```privileged``` mode.

## X2Go client

Setup a session which suit your needs and add ```ck-launch-session dbus-launch mate-session``` as session command.

### Plain

```bash
docker run --name x2go -d -p 2222:22 \
--privileged \
-v /etc/localtime:/etc/localtime \
registry.gitlab.com/morph027/x2go-mate-docker:zesty
```

### Persistent home directory

Create a folder for persistent home directory

```bash
sudo cp -R /etc/skel /home/x2go
```

```bash
docker run --name x2go -d -p 2222:22 \
--privileged \
-v /etc/localtime:/etc/localtime \
-v /home/x2go:/home/x2go
registry.gitlab.com/morph027/x2go-mate-docker:zesty
```

## Extend

### Add packages

Just refer to this image in your Dockerfile and add packages like:


```
FROM	registry.gitlab.com/morph027/x2go-mate-docker:zesty

RUN	apt-get update && \
	apt-get -y install \
		package1 \
		package2 && \
	apt-get clean && rm -rf /var/lib/apt/lists/*
```

### Theming

Theme settings will be fed from ```dconf/db/local.d/01-theme```.
